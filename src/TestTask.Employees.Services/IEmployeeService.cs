﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TestTask.Employees.Dtos.Base;
using TestTask.Employees.Dtos.Employee;

namespace TestTask.Employees.Services
{
    public interface IEmployeeService
    {
        Response<List<EmployeeDto>> GetList();
        Task<Response<EmployeeDto>> GetByIdAsync(long id);
        Task<Response<bool>> AddAsync(EmployeeDto employee);
        Task<Response<bool>> EditAsync(EmployeeDto employee);
        Task<Response<bool>> DeleteAsync(long id);
    }
}