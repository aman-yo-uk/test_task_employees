﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestTask.Employees.Dal.Db.Employees.Repositories;
using TestTask.Employees.Dtos.Base;
using TestTask.Employees.Dtos.Employee;
using TestTask.Employees.Tools.Extensions;

namespace TestTask.Employees.Services.Impl
{
    public class EmployeeService : IEmployeeService
    {
        private readonly IEmployeeRepository _employeeRepository;

        public EmployeeService(IEmployeeRepository employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        public Response<List<EmployeeDto>> GetList()
        {
            try
            {
                var res = _employeeRepository.GetDtoQuery()
                    .OrderBy(_ => _.LastName)
                    .ThenBy(_ => _.FirstName)
                    .ThenBy(_ => _.MiddleName)
                    .ToList();

                return Response<List<EmployeeDto>>.GetSuccess(res);
            }
            catch (Exception e)
            {
                return Response<List<EmployeeDto>>.GetError(e);
            }
        }

        public async Task<Response<EmployeeDto>> GetByIdAsync(long id)
        {
            try
            {
                var employee = await _employeeRepository.GetDtoByIdAsync(id);

                if (employee == null)
                    return Response<EmployeeDto>.GetError("Сотрудник не найден");

                return Response<EmployeeDto>.GetSuccess(employee);
            }
            catch (Exception e)
            {
                return Response<EmployeeDto>.GetError(e);
            }
        }

        private async Task<string> ValidateEmployeeAsync(EmployeeDto employee)
        {
            return await Task.Run(() =>
            {
                if (employee == null)
                    return "Не получены данные о сотруднике";

                if (employee.LastName.IsNullOrEmpty())
                    return "Не заполнена фамилия";

                if (employee.FirstName.IsNullOrEmpty())
                    return "Не заполнено имя";

                if (_employeeRepository.GetDtoQuery()
                    .Where(_ => _.Id != employee.Id && _.LastName == employee.LastName && _.FirstName == employee.FirstName && _.MiddleName == employee.MiddleName)
                    .Any())
                    return "Сотрудник с указанным ФИО уже существует";

                return null;
            });
        }

        public async Task<Response<bool>> AddAsync(EmployeeDto employee)
        {
            try
            {
                var validateRes = await ValidateEmployeeAsync(employee);
                if (validateRes.HasValue())
                    return Response<bool>.GetError(validateRes);

                employee.Id = 0;
                await _employeeRepository.SaveDtoAsync(employee);

                return Response<bool>.GetSuccess(true);
            }
            catch (Exception e)
            {
                return Response<bool>.GetError(e);
            }
        }

        public async Task<Response<bool>> EditAsync(EmployeeDto employee)
        {
            try
            {
                var validateRes = await ValidateEmployeeAsync(employee);
                if (validateRes.HasValue())
                    return Response<bool>.GetError(validateRes);

                await _employeeRepository.SaveDtoAsync(employee);

                return Response<bool>.GetSuccess(true);
            }
            catch (Exception e)
            {
                return Response<bool>.GetError(e);
            }
        }

        public async Task<Response<bool>> DeleteAsync(long id)
        {
            try
            {
                var employee = await _employeeRepository.GetDtoByIdAsync(id);

                if (employee == null)
                    return Response<bool>.GetError("Сотрудник не найден");

                await _employeeRepository.DeleteDtoAsync(employee);

                return Response<bool>.GetSuccess(true);
            }
            catch (Exception e)
            {
                return Response<bool>.GetError(e);
            }
        }
    }
}