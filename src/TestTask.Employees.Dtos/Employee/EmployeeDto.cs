﻿using TestTask.Employees.Dtos.Base.Impl;

namespace TestTask.Employees.Dtos.Employee
{
    public class EmployeeDto : BaseDto
    {
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string Position { get; set; }
    }
}