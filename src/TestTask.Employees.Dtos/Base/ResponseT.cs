﻿using System;

namespace TestTask.Employees.Dtos.Base
{
    /// <summary>
    /// Базовый ответ типизированный
    /// </summary>
    public class Response<T> : Response
    {
        /// <summary>
        /// Ответ метода сервиса
        /// </summary>
        public T Result { get; set; }

        public static Response<T> GetSuccess(T result)
        {
            return new Response<T>
            {
                IsSuccess = true,
                Result = result
            };
        }

        public static Response<T> GetError(string errorMessage = null, T result = default(T))
        {
            return new Response<T>
            {
                IsSuccess = false,
                ErrorMessage = errorMessage,
                Result = result
            };
        }

        public static Response<T> GetError(Exception ex = null, T result = default(T))
        {
            return new Response<T>
            {
                IsSuccess = false,
                ErrorMessage = ex.ToString(),
                Result = result
            };
        }
    }
}