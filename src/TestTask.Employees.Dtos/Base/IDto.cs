﻿namespace TestTask.Employees.Dtos.Base
{
    public interface IDto
    {
        long Id { get; set; }
    }
}