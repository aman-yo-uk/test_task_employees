﻿using Microsoft.EntityFrameworkCore;

namespace TestTask.Employees.Dal.Db.Base
{
    public interface IUow<TDbContext> where TDbContext : DbContext
    {
        TDbContext GetDbContext();
    }
}