﻿using Microsoft.EntityFrameworkCore;

namespace TestTask.Employees.Dal.Db.Base.Impl
{
    public class Uow<TDbContext> : IUow<TDbContext> where TDbContext : DbContext, new()
    {
        public TDbContext Db { get; private set; }

        public TDbContext GetDbContext()
        {
            return Db ??= new TDbContext();
        }
    }
}