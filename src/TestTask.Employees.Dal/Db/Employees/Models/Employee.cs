﻿using TestTask.Employees.Dal.Db.Base;

namespace TestTask.Employees.Dal.Db.Employees.Models
{
    public class Employee : IDomainModel
    {
        public long Id { get; set; }
        public bool IsDeleted { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string Position { get; set; }
    }
}