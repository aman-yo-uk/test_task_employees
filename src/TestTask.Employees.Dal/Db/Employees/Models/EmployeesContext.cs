﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace TestTask.Employees.Dal.Db.Employees.Models
{
    public class EmployeesContext : DbContext
    {
        public DbSet<Employee> Employees { get; set; }

        public EmployeesContext()
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            optionsBuilder.UseNpgsql(configuration.GetConnectionString("DefaultConnection"));
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Employee>(EmployeeConfigure);
        }

        protected void EmployeeConfigure(EntityTypeBuilder<Employee> builder)
        {
            builder
                .Property(_ => _.IsDeleted)
                .HasDefaultValue(false);

            builder
                .Property(_ => _.LastName)
                .IsRequired();

            builder
                .Property(_ => _.FirstName)
                .IsRequired();
        }
    }
}