﻿using System;
using System.Linq.Expressions;
using TestTask.Employees.Dal.Db.Base;
using TestTask.Employees.Dal.Db.Base.Impl;
using TestTask.Employees.Dal.Db.Employees.Models;
using TestTask.Employees.Dtos.Employee;

namespace TestTask.Employees.Dal.Db.Employees.Repositories.Impl
{
    public class EmployeeRepository : SimpleRepository<EmployeesContext, Employee, EmployeeDto>, IEmployeeRepository
    {
        private readonly IUow<EmployeesContext> _uow;

        public EmployeeRepository(IUow<EmployeesContext> uow) : base(uow)
        {
            _uow = uow;
        }

        public override Func<EmployeeDto, Employee, Employee> ToDomainModel()
        {
            return (_, source) => new Employee
            {
                Id = source != null ? source.Id : _.Id,
                LastName = _.LastName,
                FirstName = _.FirstName,
                MiddleName = _.MiddleName,
                Position = _.Position
            };
        }

        public override Expression<Func<Employee, EmployeeDto>> ToDto()
        {
            return _ => new EmployeeDto
            {
                Id = _.Id,
                LastName = _.LastName,
                FirstName = _.FirstName,
                MiddleName = _.MiddleName,
                Position = _.Position
            };
        }
    }
}