﻿using TestTask.Employees.Dal.Db.Base;
using TestTask.Employees.Dal.Db.Employees.Models;
using TestTask.Employees.Dtos.Employee;

namespace TestTask.Employees.Dal.Db.Employees.Repositories
{
    public interface IEmployeeRepository : ISimpleRepository<Employee, EmployeeDto>
    {
    }
}