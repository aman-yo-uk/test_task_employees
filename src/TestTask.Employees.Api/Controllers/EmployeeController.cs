﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TestTask.Employees.Dtos.Base;
using TestTask.Employees.Dtos.Employee;
using TestTask.Employees.Services;

namespace TestTask.Employees.Api.Controllers
{
    [Route("api/[controller]")]
    public class EmployeeController : BaseController
    {
        private readonly IEmployeeService _employeeService;

        public EmployeeController(
            IEmployeeService employeeService
            )
        {
            _employeeService = employeeService;
        }

        [HttpGet]
        [Route("")]
        public Response<List<EmployeeDto>> Get()
        {
            return _employeeService.GetList();
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<Response<EmployeeDto>> Get(long id)
        {
            return await _employeeService.GetByIdAsync(id);
        }

        [HttpPost]
        [Route("add")]
        public async Task<Response<bool>> Add(EmployeeDto employee)
        {
            return await _employeeService.AddAsync(employee);
        }

        [HttpPost]
        [Route("edit")]
        public async Task<Response<bool>> Edit(EmployeeDto employee)
        {
            return await _employeeService.EditAsync(employee);
        }

        [HttpPost]
        [Route("del")]
        public async Task<Response<bool>> Delete(long id)
        {
            return await _employeeService.DeleteAsync(id);
        }
    }
}